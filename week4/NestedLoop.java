package week4;

public class NestedLoop {

    public static void main(String[] args) {
        outer:for (int i = 0; i < 3; i++) {
            for (int j = 2; j < 5; j++) {
                if (j == 4)
                    break outer;
                System.out.println(i + "," + j);
            }

        }
    }
}
