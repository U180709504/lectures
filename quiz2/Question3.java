package quiz2;

public class Question3 {
    public static void main(String[] args) {
        int x = 3;
        while (x > 0) {
            int y = 0;
            while (y < 4) {
                System.out.println("x = " + x + " y = " + y);
                y += 2;
            }
            x -= 1;
        }

    }
}
