package week10;

import java.util.Calendar;
import java.util.Date;

public class Person {

    private String name;
    private String gender;
    private Date birthday;
    private String surname;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getAge(Date today){
        Calendar calToday = Calendar.getInstance();
        calToday.setTime(today);

        Calendar calBirth =  Calendar.getInstance();
        calBirth.setTime(getBirthday());

        return calToday.get(Calendar.YEAR) - calBirth.get(Calendar.YEAR);
    }
}
