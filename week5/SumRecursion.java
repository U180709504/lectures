package week5;

public class SumRecursion {

    public static void main(String[] args) {
        int  n = 10;

        System.out.println(sumLoop(n));
        System.out.println(sumRec(n));

    }

    public static int sumLoop(int n){
        int sum = 0;
        for (int  i = 1; i <=n; i++){
            sum += i;
        }
        return sum;
    }

    public static int sumRec(int n){
        if (n == 1)
            return 1;

        return n + sumRec(n-1);
    }


    public static int factorial(int n){
        if (n == 0)
            return 1;
        return n * factorial(n-1);
    }
}
