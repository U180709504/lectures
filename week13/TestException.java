package week13;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class TestException {

    public static void main(String[] args){
        int[] arr = new int[2];
        try {
            int[][][] threed = new int[1000000][1000000][100000];
        }catch(Error e){
            System.out.println("Memory error");
            e.printStackTrace();
        }

        arr[1] = 5;
        System.out.println("After Exception");

        System.out.println( 5 / 1);


        try {
            BufferedReader bf = new BufferedReader(new FileReader("text.txt") );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    public static void abc() throws RuntimeException{
        String str = null;

        if (str == null){
            throw new RuntimeException("null");
        }
        System.out.println(str.length());
    }

}
