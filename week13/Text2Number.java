package week13;

import java.util.HashMap;
import java.util.Map;

public class Text2Number {
    public static final String MINUS ="minus";

    Map<String,Integer> map = new HashMap();
    public Text2Number(){
        map.put("zero" , 0);
        map.put("one" , 1);
        map.put("two" , 2);
        map.put("three" , 3);
        map.put("four" , 4);
        map.put("five" , 5);
        map.put("six" , 6);
        map.put("seven" , 7);
        map.put("eight" , 8);
        map.put("nine" , 9);
        map.put("ten" , 10);
        map.put("twenty" , 20);
        map.put("thirty" , 30);
        map.put("fourty" , 40);
        map.put("fifty" , 50);
        map.put("sixty" , 60);
        map.put("seventy" , 70);
        map.put("eighty" , 80);
        map.put("ninety" , 90);
    }

    public int convert(String text) throws InvalidNumberException{
        int number = 0;
        String[] tokens = text.split(" ");
        int i = 0;
        boolean negative = tokens[0].equals(MINUS);

        if (negative) i++;
            while (i < tokens.length) {
                Integer value = map.get(tokens[i]);
                if (value == null) {  //invalid input
                    throw new InvalidNumberException("not a valid parameter");
                }
                if (value != null)
                    number += value;
                i++;
            }

        if (negative)
            number = -number;
        return number;
    }
}
