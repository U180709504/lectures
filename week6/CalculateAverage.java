package week6;

import java.util.Scanner;

public class CalculateAverage {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Student[] students = new Student[3];


        for (int i =0; i < 3; i++){
            Student st = new Student();
            System.out.print("Enter student id:");
            String id = scanner.next();
            st.id = id;
            System.out.print("Enter midterm exam grade: ");
            int midtermExam = scanner.nextInt();
            st.midtermScore = midtermExam;
            System.out.print("Enter final exam grade: ");
            int finalExam = scanner.nextInt();
            st.finalScore = finalExam;

            students[i] = st;
        }

        int sum = 0;
        for (int i =0; i < 3; i++){
            sum+= students[i].calculateOverAllScore();
        }

        System.out.println("Class Average = " + sum/3);

    }




}
