package week9;

public class ParemeterPassDemo {

    int value;

    public static void main(String args[]){
        int a = 5;
        //int copyA = a;
        //copyA++;   method body
        increment(a);
        System.out.println("a after increment = " + a);

        int[] arrAddress = {1};

        int[] copyOfArrAddress = arrAddress;

        copyOfArrAddress[0] = 5;
        System.out.println("arrAddress[0] = " +  arrAddress[0]);


        //arr[0] and value[0] are the same
        //value[0]++;

        System.out.println("after value increment arr[0] = " +arrAddress[0]);

        increment(arrAddress);
        System.out.println("array element after increment = " +arrAddress[0]);

        //modify(arr);
        //System.out.println("array element after modify = " +arr[0]);

        ParemeterPassDemo pp = new ParemeterPassDemo();
        pp.value = 5;

        increment(pp);

        System.out.println("After Incerement pp = " + pp.value);

        modify(pp);

        System.out.println("After modify pp = " + pp.value);
    }

    private static void increment(ParemeterPassDemo copyVar) {
        copyVar.value++;
    }

    private static void modify(ParemeterPassDemo copyVar) {
        copyVar = new ParemeterPassDemo();
        copyVar.value = 1;
    }


    public static void increment(int copyA){
        copyA = copyA + 1;
        System.out.println("value in increment = " + copyA);
    }

    public static void increment(int[] values){
        values[0]++;
        System.out.println("array element in increment = " + values[0]);
    }

    public static void modify(int values[]){
        values = new int[1];
        values[0] = 9;
    }

}
