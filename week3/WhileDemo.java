package week3;

public class WhileDemo {

    public static void main(String[] args) {
        char c = 'A';

        while (c <= 'z'){
            System.out.print( c + " ");
            c++;
        }

    }
}
