package week3;

public class VariableScope {

    public static void main(String[] args) {


        int x = 10;
        if (x == 10){
            int y = 11;
            System.out.println("x = " + x + " y = " + y);
        }
        //y = y +1; error
    }
}
