package week3;

public class UnaryDemo2 {

    public static void main(String[] args) {
        int a = 5;
        int b = a++;   // b=5  a = 6
        int c = ++a;   // a = 7; c = 7;
        int d = a++ + b-- + c++; // a= 8 b=4 c =8 d = 19

        System.out.println(a + " " + b +" " + c + " " + d);
    }
}
