package week12.drawing.main;


import week12.drawing.shapes.*;

public class TestDrawing {

	public static void main(String[] args) {
		
		Drawing drawing = new Drawing();
		
		drawing.addDrawable(new Circle(5));
		drawing.addDrawable(new Circle(3));
		drawing.addDrawable(new Rectangle(5,6));
		drawing.addDrawable(new Rectangle(2,7));
		drawing.addDrawable(new Rectangle(2,5));

		drawing.addDrawable(new Square(2));

		drawing.addDrawable(new Triangle(4,6));
		//drawing.addDrawable("Hello");

		//Shape s = new Shape();

		System.out.println("Total area = " + drawing.calculateTotalArea());

		drawing.addDrawable(new Text("Hello"));

		drawing.draw();

	}

}