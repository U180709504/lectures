package week12.drawing.main;

import week12.drawing.shapes.*;

import java.util.ArrayList;

public class TestCasting {

    public static void main(String[] args){

        Object obj = new Circle(5);
        Shape s = new Circle(5);
        Drawable t = new Circle(5);

        Drawable d = new Text("Hello");

        //obj.area();
        Circle c = ((Circle)obj);

        if (obj instanceof Rectangle)
            ((Rectangle)obj).area();

        //((Circle)obj).area();
        c.area();

        ArrayList<Object>  list = new ArrayList<>();

        list.add(new Circle(6));
        list.add("Hello");

    }
}
