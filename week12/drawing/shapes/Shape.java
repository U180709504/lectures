package week12.drawing.shapes;

public  abstract class Shape implements Drawable{

    public abstract double area();

}
