package week12.drawing.shapes;

public interface Drawable {
    void draw();
}
