package week11;

public class Student {

    private String id;
    private String name;

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Student) {
            Student s = (Student) obj; // explain later
            return id == s.id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
