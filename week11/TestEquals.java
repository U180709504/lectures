package week11;

import week1.HelloWorld;

public class TestEquals {

    public static void main(String[] args) {

        Student s1 = new Student("123", "Ozgur");

        System.out.println(s1.getClass());

        Student s2 = new Student("123", "Ozgur");

        if (s1.equals(s2)){  //s1.equals("Example") raises exception
            System.out.println("same student");
        }else{
            System.out.println("different student");
        }

        String str1 = "Hello";
        String str2 = "Hell";
        str2 = str2 +"o";

        System.out.println(str1);
        System.out.println(str2);
        if(str1 == str2){
            System.out.println("==");
        }else{
            System.out.println("!=");
        }
        if(str1.equals(str2)){
            System.out.println("==");
        }else{
            System.out.println("!=");
        }

        String str3 = new String("Hello");
        if(str1 == str3){
            System.out.println("==");
        }else{
            System.out.println("!=");
        }
    }
}
