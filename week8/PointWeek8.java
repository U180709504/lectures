package week8;

public class PointWeek8 {
    private int x;
    private int y;

    public PointWeek8(){
        this(3,3);
    }

    public PointWeek8(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static void main(String args[]){
        PointWeek8 p1 = new PointWeek8();

        PointWeek8 p2 = new PointWeek8(3, 4);


    }
}
