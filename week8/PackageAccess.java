package week8;

import tr.com.abc.drawing.Point;
import week6.Student;
import java.lang.String;
import java.util.ArrayList;

public class PackageAccess {

    public static void main(String args[]){

        String str = "Hello";
        ArrayList list;

        Point p = new Point();
        tr.com.xyz.drawing.Point p2 = new tr.com.xyz.drawing.Point();

        ScopeDemo demo = new ScopeDemo(); // no need to import in the same package

        Student s = new Student();
    }
}
