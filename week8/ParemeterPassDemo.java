package week8;

public class ParemeterPassDemo {

    public static void main(String args[]){
        int a = 5;
        //int value = a;
        //value++;
        increment(a);
        System.out.println("a after increment = " + a);

        int[] arr = {1};

        int[] value = arr;
        value = new int [1];
        value[0] = 5;

        //arr[0] and value[0] are the same
        value[0]++;

        System.out.println("after value increment arr[0] = " +arr[0]);

        increment(arr);
        System.out.println("array element after increment = " +arr[0]);

        modify(arr);
        System.out.println("array element after modify = " +arr[0]);
    }

    public static void increment(int value){
        value = value + 1;
        System.out.println("value in increment = " + value);
    }

    public static void increment(int[] values){
        values[0]++;
        System.out.println("array element in increment = " + values[0]);
    }

    public static void modify(int values[]){
        values = new int[1];
        values[0] = 9;
    }

}
