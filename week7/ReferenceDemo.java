package week7;

public class ReferenceDemo {
    public static void main(String[] args) {

        int a = 3;
        int b = 3;

        System.out.println(a == b);

        int[] arr1 = {3};
        int[] arr2 = {3};

        System.out.println(arr1 == arr2);
        System.out.println(arr1);
        System.out.println(arr2);
        arr2 = arr1;
        System.out.println(arr1 == arr2);
        arr1[0] = 4;
        System.out.println(arr1[0]);

        Point p1 = new Point(10,10);
        Point p2 = new Point(10,10);

        System.out.println(p1==p2);

        p2 = p1;
        p1.setX(15);

        System.out.println(p2.getX());

        System.out.println(p1 == p2);


        String str1 = "Hello";
        String str2 = "HelloW".substring(0,5);

        System.out.println(str2);
        System.out.println(str1 == str2);
        System.out.println(str1.equals(str2));


    }
}
